+++
date = "2017-04-17T11:01:21-04:00"
draft = false
title = "Overview"
type = "developer"
anchor = "overview"
+++

# Overview
PrimedIO is different from black-box personalisation platforms. Most other platforms take your raw data, and return `predictions` ('*recommendations as a commodity*'). By doing so, your data and the value created from your data no longer belong to you. 

PrimedIO, in contrast, receives `predictions` and makes those available to your end-users by integrating with your customer-outlets. This means that you decide how these `predictions` are created: `predictions` can be the output of your data scientists, externally sourced, manually curated lists, one of our prepackaged Machine Learning (ML) models or a combination of the aforementioned. Either way, you remain in control of both your data ánd your predictive artifacts. 

![alt text](/images/dev-overview.png "PrimedIO overview")

By defining a generic framework for delivering `predictions` and handling the concerns about latency, security and scalability, PrimedIO enables fast ML iterations and opens up possibilities for personalisation including and beyond recommendations.

# Concepts
The core of PrimedIO is that we define sets of `signals` and sets of `targets`. The two are connected through relationships, or `predictions`. A `signal` is usually something that we deduce from a customer event or interaction: __the observed variable__. Examples of `signals` include device type, cookieId, userId, time of day, and more. `targets` then are our __dependent variables__, the things we want to predict and use to vary customer experience among our users. 

At this time, `targets` can only be discrete and need to represent an 'actionable' output on the customer-facing front-end.

Actionable means that a `target` usually does not map entirely with what a data scientist would call a __dependent variable__. Typically, a __dependent variable__ corresponds to a hypothesis ('*Alice will cancel her contract next month*', '*Bob likes Total Recall*' or '*Chris is a male between 20 and 40 years old*'). `predictions` as conventionally understood then assign probabilities to these hypotheses using some statistical artifact. These outcomes, however useful, do not represent 'actionable' `targets`: populating PrimedIO with this data will inevitably lead to adding additional logic in the front-end:

```sql
if churnProbability > 0.5 then showPopup();
```

Doing this frequently and over various platforms will make it next to impossible to systematically measure the impact of your models on KPI's. As a result front-end implementation and data science R&D style development often become unneccesarily tangled, slowing down development on both sides. Instead, `targets` map to the last bit of logic, where the model outcomes are joined with actions that impact the user: `showPopup();` in the above contrived example.

`predictions` connect the two together; for each signal there will be a `prediction` for each `target` with an associated score (usually the output of some statistical model, but sometimes this is provided by a human actor such as the product owner).

![alt text](/images/dev-signal-target-prediction.png "PrimedIO signals, targets and predictions")

Generally, there are two ways of interacting with PrimedIO: (1) mutating the set of predictions and (2) querying the set of predictions for a given campaign. The input for PrimedIO queries (or, _personalise calls_) is a set of `signals` associated with a certain event and the output (or what is returned by PrimedIO) of the personalise call will be a list of `targets` ordered by their blended, predicted relevance given those inputs.

{{% block note %}} PrimedIO defines models as a set of relationships between an __observed variable__ and an __actionable outcome__. Each relationship, or `prediction`, represents the predicted relevance of the outcome given observed variable.{{% /block %}}

## Features and Signals
`features` represent raw event properties as logged during a user event or interaction. You can specify custom `features` or use the predefined ones:

-----------------------------------------------------------------
| name | type |  description | example |
| ---- | ---- | ------------ | ------- |
| `now` | Date object | local timestamp of the event | `Tue Jul 03 2018 07:59:46 GMT+0200 (CEST)` |
| `ua` | string | useragent string | `Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) ....` |
| `vuuid` | string | unique identifier for this pageview | `e47bf759-705d-424d-ab3c-c09979e173c9` |
| `duuid` | string | unique identifier for this device | `e47bf759-705d-424d-ab3c-c09979e173c9` |
| `url` | string | current url | `https://www.example.com/home` |
-----------------------------------------------------------------

From these `features` you derive categorical `signals` using detectors, or you provide fixed values such as 'cookieId' or 'userId'. Example 'signals' include 'deviceType' (derived from `ua`), 'cookieId' (fixed value) or 'dayOfTheWeek' (derived from `now`). 

```python
s0 = { 'key': 'iphone', 'type': 'deviceType' }
s1 = { 'key': 'alice' , 'type': 'userId' }
```

For `signals` to succesfully trigger `predictions`, they can't only be known at query time. Data scientists will need to use the same events and properties to train their models. So logging the above `features`, either using the PrimedIO tracking facilities or a third-party solution, and storing the data in a data lake is therefore necessary. Data scientists can then use the very same inputs that trigger `predictions`, to match with their models consistently.

## Targets
Actionable outcomes are represented by `targets`. A `target` in PrimedIO is represented by a 'key' and a 'value'. The key should be unique for the `target` within a `universe`. The value is a freeform dictionary and can contain anything you like: HTML content, item ID's, properties, etc.

```python
t0 = { 'key': '123', 'value': { 'html': 'alert("this popup brought to you by PrimedIO");' } }
t1 = { 'key': '456', 'value': { 'id': 'myid' } }
t2 = { 'key': '789', 'value': { 'img': 'http://stylus-lang.com/img/octocat.svg' } }
```

## Predictions
A prediction describes the predicted relevance between a given `signal` and `target` pair.

```python
prediction = { 'sk': 'iphone', 'tk': '123', 'score': 0.75 }
```

## Models

## Universes

## Experiments

## Campaigns

## A/B Variants


# Implementation
The PrimedIO engine stores predictions and meta-information such as about campaigns, A/B testing, Apikeys and the like. Generally, implementing PrimedIO involves three different stakeholders: 

1. Front-end developers: this is where the results from PrimedIO will ultimately end up, and be used to construct a personalised experience for the end-user. Typically, the front-end engineer and the data scientist sit together to determine which `signals` will be made available (e.g. 'userId', 'cookieId'), and what output `targets` shall be used. 
2. Data scientists: With the above agreed on `signals` the data scientist can train any model and map the `predictions` from `signals` to `targets`. After signing off the model using offline validation metrics the `predictions` can be uploaded to PrimedIO.
2. Data engineers: Data engineers use the data scientists' pipeline to regularly upload predictions to PrimedIO, or implement a streaming pipeline to update PrimedIO in real-time.

Additionally, the REST api and admin gui are available to facilitate specific use cases, and easier management.

## Front-end
Client-side integrations can use the JavaScript and Objective-C libraries, or resort to calling the REST api directly. All libraries expose two endpoints: `personalise` and `convert` and collect `signals` to be sent along with the calls. Optionally, the libraries can also perform real-time behavioral tracking.

### Installation
{{% tabs %}}
{{% tab "JavaScript" %}}
```html
<script src="https://s3-eu-west-1.amazonaws.com/cdn.primed.io/2.1.0-beta/primedio.js"></script>
```
{{% /tab %}}
{{% tab "NodeJS" %}}
```shell
npm install primednodejs
```
{{% /tab %}}
{{% tab "Objective-C" %}}
```objective-c
To be added
```
{{% /tab %}}
{{% /tabs %}}

### Initialization
{{% tabs %}}
{{% tab "JavaScript (New & Classic)" %}}
```javascript
const pio = PRIMED({
    connectionString: "https://primed.io",
    nonce: new Date().toISOString(),
    publicKey: "xxxxxxxxxxxxxxxxxxxxxxxx",
    signature: "xxxxxxxxxxxxxxxxxxxxxxxx",
});
```
{{% /tab %}}
{{% tab "NodeJS" %}}
```javascript
var PRIMEDIO = require('primednodejs');

var pio = new PRIMEDIO({
    api_url: "https://primed.io",
    public_key: 'xxxxxxxxxxxxxxxxxxxxxxxx', 
    secret_key: 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
});
```
{{% /tab %}}
{{% tab "Objective-C" %}}
```objective-c
To be added
```
{{% /tab %}}
{{% /tabs %}}

In order for JavaScript client-side applications to make authorized calls, it expects two server-side generated values: `nonce` and `signature`. The signature is generated by concatenating the `publicKey`, the `secretKey` and the `nonce`. The publicKey and secretKey are provided via your PrimedIO contact. The `nonce` is the Unix timestamp in seconds from midnight 1970 UTC of the moment at which you are generating your signature. The concatenation of these three values is then sha512 hashed and the resulting value is the signature.

For the NodeJS, and Objective-C implementations this step is not needed.

```python
# SERVER-SIDE PYTHON EXAMPLE CODE
nonce = int(datetime.datetime.now(datetime.timezone.utc).timestamp())
local = hashlib.sha512()
signature = "{}{}{}".format(pubkey, secretkey, nonce)
local.update(signature.encode('utf-8'))
signature = local.hexdigest()
```

### Use
{{% tabs %}}
{{% tab "JavaScript (New)" %}}
```html
<div id="$pio.frontpage.recommendations">
    <!-- 
    placeholder where successful calls will render personalised elements 
    typically, this div is not empty to begin with but contains reasonable 'fallback' content which is subsequently overwritten when the personalise calls succeed
    -->
</div>
```
```javascript
pio.detectors.bind({userId: 'alice'});

pio.campaigns.bind({
    key: 'frontpage.recommendations', // this should match div id suffix
    limit: 5,
    fn: ({ value, rank, ruuid, el, result }) => {
        // perform page rendering here
    }
});

pio.render();
```
{{% /tab %}}
{{% tab "JavaScript (Classic)" %}}
```javascript
pio.personalise({
    campaign: 'frontpage.recommendations',
    signals: { userId: 'ALICE'},
    limit: 5,
    abvariant: 'A' // optional
}).then( value => {
    console.log(value);
    // expected output: "An array of result objects"
}).catch( reason => {
    console.error( 'something went wrong: ', reason );
});
```
{{% /tab %}}
{{% tab "NodeJS" %}}
```javascript
var promise = pio.personalise({
    campaign: 'frontpage.recommendations',
    signals: { 
        'userId': 'ALICE'
    },
    limit_results: 5,
    abvariant: 'A' // optional
});

promise.then(function(response) {
    // 'response' is a wrapper object,
    // calling 'response.all()' returns
    // the list of results which can then be
    // used to render the page
    var results = response.all();
}).catch(function (err) {
    console.log('Personalise failed: ' + err);
});
```
{{% /tab %}}
{{% tab "Objective-C" %}}
```objective-c
NSDictionary *dictSignals = @{@"userId": @"ALICE"};

[primed personalise:@"frontpage.recommendations" signals:dictSignals limit:5 success:^(NSDictionary *response) {    
    //Handle response
} failed:^(NSString *message) {
    //Handle message
}];
```
{{% /tab %}}
{{% /tabs %}}

#### Detectors.bind()
Detectors provide context which is in turn used upon calling the Primed backend for personalisation calls. Each detector delivers a 'signal' value that corresponds to the signals used to train the model. A detector can be a fixed value, or a function which returns a value.

```javascript
pio.detectors.bind({
    userId: function() { return 'alice'; }
});
```

| arg | type | required | description | example |
| --- | ---- | -------- | ------ | ------- |
| key | string | Yes | signal type name | `userId` |
| value | literal OR function | Yes | either a literal value or a function that returns a literal value | `alice` OR `function() { return 'alice'; }` |

#### Campaigns.bind()
For each bound campaign, primedjs will call the PrimedIO backend asynchronously retrieving the corresponding results. All signals (derived from the detectors) will be sent along with the requests.

```javascript
pio.campaigns.bind({
    key: 'frontpage.recommendations',
    limit: 5,
    fn: ({ value, rank, ruuid, el, result }) => {
        // create a new HTML node
    }
});
```

| arg | type | required | description | example |
| --- | ---- | -------- | ------ | ------- |
| key | string | Yes | campaign key | `frontpage.recommendations` |
| limit | integer | No (defaults to 5) | number of results to return | `10` |
| fn | function | Yes | asynchronous callback function that fires for each result as it is returned by the PrimedIO backend | `fn: ({ value, rank, ruuid, el, result }) => { /* process result */ }` |

### Mark results as converted
Upon successful personalisation, a list of results will be returned. Each result will contain a unique RUUID (Result UUID). It is used to track the conversion of this particular outcome, which is used to identify which of the A/B variants performed best. Conversion is also generic, but generally we can associate this with clicking a particular outcome. In order for PrimedIO to register this feedback, another call needs to be made upon conversion. This in turn allows the system to evaluate the performance (as CTR) of the underlying Model (or blend).

{{% tabs %}}
{{% tab "JavaScript (New & Classic)" %}}
```javascript
pio.convert({
    ruuid: "6d2e36d1-1b58-4fbc-bea8-868e3ec11c87",
    data: { heartbeat: 0.1 }
});
```
{{% /tab %}}
{{% tab "NodeJS" %}}
```javascript
pio.convert({
    ruuid: "6d2e36d1-1b58-4fbc-bea8-868e3ec11c87"
}); 
```
{{% /tab %}}
{{% tab "Objective-C" %}}
```objective-c
NSDictionary *data = @{ @"heartbeat": @"0.1" };
[primed convert:@"6d2e36d1-1b58-4fbc-bea8-868e3ec11c87" data:dictData];
```
{{% /tab %}}
{{% /tabs %}}

| arg | type | required | description | example |
| --- | ---- | -------- | ------ | ------- |
| ruuid | string | Yes | ruuid for which to register conversion | `"6d2e36d1-1b58-4fbc-bea8-868e3ec11c87"` |
| data | dictionary | No | freeform data payload | `{ heartbeat: 0.1 }` |

## Data engineers
### Installation
{{% tabs %}}
{{% tab "Python" %}}
```python
pip install pyprimed --extra-index-url http://pypi-staging.primed.io --trusted-host pypi-staging.primed.io
```
{{% /tab %}}
{{% /tabs %}}

### Initialization
{{% tabs %}}
{{% tab "Python" %}}
```python
from pyprimed.pio import Pio

# instantiate library using connection uri
pio = Pio(uri='http://<user>:<password>@<api_url>:<port>')
```
{{% /tab %}}
{{% /tabs %}}

### Use
{{% tabs %}}
{{% tab "Python" %}}
```python
# fetch an existing universe
u = pio\
    .universes\
    .filter(name='myuniverse')\
    .first

# fetch an existing model
m = pio\
    .model\
    .filter(name='mymodel')\
    .first

# create mapping from signals to targets
mapping = [
    {"sk": "alice", "tk": "article1", "score": 0.65},
    {"sk": "alice", "tk": "article2", "score": 0.25}
]

# upsert mapping
pio\
    .predictions\
    .upsert(mapping)
```
{{% /tab %}}
{{% /tabs %}}

# Features
## Tracking
## Search
## Delivery
## Analytics

# Quick start
## Initialize
{{% tabs %}}
{{% tab "Python" %}}
```python
from pyprimed.pio import Pio

pio = Pio(uri='http://<user>:<password>@<api_url>:<port>')
```
{{% /tab %}}
{{% /tabs %}}

## Create a Universe
{{% tabs %}}
{{% tab "Python" %}}
```python
u = pio.universes.create(name='myfirstuniverse')
```
{{% /tab %}}
{{% /tabs %}}


## Create a Model
{{% tabs %}}
{{% tab "Python" %}}
```python
m = pio.models.create(name='myfirstmodel')
```
{{% /tab %}}
{{% /tabs %}}

## Upload predictions
{{% tabs %}}
{{% tab "Python" %}}
```python
# prepare a set of predictions
# WARNING: `sk` and `tk` should always be a string!
predictions = [
    {'sk': 'ALICE', 'tk': 'ARTICLE-1', 'score': 0.35},
    {'sk': 'BOB', 'tk': 'ARTICLE-1', 'score': 0.75}, 
    {'sk': 'CHRIS', 'tk': 'ARTICLE-1', 'score': 0.15},
    {'sk': 'ALICE', 'tk': 'ARTICLE-2', 'score': 0.25},
    {'sk': 'BOB', 'tk': 'ARTICLE-2', 'score': 0.15}, 
    {'sk': 'CHRIS', 'tk': 'ARTICLE-3', 'score': 0.43},
    {'sk': 'ALICE', 'tk': 'ARTICLE-3', 'score': 0.69},
    {'sk': 'BOB', 'tk': 'ARTICLE-3', 'score': 0.13}, 
    {'sk': 'CHRIS', 'tk': 'ARTICLE-3', 'score': 0.01}
]

# retrieve our universe
u = pio.universes.filter(name='myfirstuniverse').first

# retrieve our model
m = pio.models.filter(name='myfirstmodel').first

# upload predictions for this model and universe
pio\
    .predictions\
    .on(model=m, universe=u)\
    .upsert(predictions)
```
{{% /tab %}}
{{% /tabs %}}

## Create a campaign
{{% tabs %}}
{{% tab "Python" %}}
```python
from pyprimed.models.abvariant import CustomAbvariant, CONTROL

# retrieve our universe
u = pio.universes.filter(name='myfirstuniverse').first

# retrieve our model
m = pio.models.filter(name='myfirstmodel').first

# create a campaign
c = u.campaigns.create(key='myfirstcampaign', name='something descriptive')

# create an experiment for which we will define abvariants
e = c.experiments.create(name='myfirstexperiment')

# define an abvariant with only one model
ab = CustomAbvariant(label='A', models={m: 1.0})

# attach the abvariant to the experiment, ensuring 90% of traffic will flow to
# the abvariant, and 10% will flow to CONTROL
e.abvariants.create({ab: 0.9, CONTROL: 0.1})
```
{{% /tab %}}
{{% /tabs %}}

## Personalise
{{% tabs %}}
{{% tab "Python" %}}
```python
u = pio.universes.filter(name="myfirstuniverse").first
c = u.campaigns.filter(key='myfirstcampaign').first

c.personalise(
  pubkey='mypubkey',
  secretkey='mysecretkey',
  signals={'userid': 'BOB'},
  limit=5,
  abvariant='A' // optional
)
```
{{% /tab %}}
{{% tab "JavaScript" %}}
```javascript
pio.personalise({
    campaign: "myfirstcampaign",
    signals: { userid: 'BOB'},
    limit: 5,
    abvariant: 'A' // optional
}).then( value => {
    console.log(value);
    // expected output: "An array of result objects"
}).catch( reason => {
    console.error( 'something went wrong: ', reason );
});
```
{{% /tab %}}

{{% tab "Objective-C" %}}
```objective-c
NSDictionary *dictSignals = @{@"userid": @"BOB"};

[primed personalise:@"myfirstcampaign" signals:dictSignals limit:5 success:^(NSDictionary *response) {    
    //Handle response
} failed:^(NSString *message) {
    //Handle message
}];
```
{{% /tab %}}
{{% /tabs %}}

# Advanced

## Blending

## Dithering

## Recency boosting

## Sticky A/B testing