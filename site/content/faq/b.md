+++
title = "Will there be vendor lock-in?"
date = "2018-08-14T12:42:14+05:30"
draft = true
+++

Absolutely not. You’re in control, and you’re free as a bird! Your data, prediction models and expertise stay with you, as it should be. Client libraries (including the tracking component) are open source (Apache 2.0 license) and the delivery system works via standard REST. 

If you want to incorporate your own tracking, delivery or analytics components, you can do so easily. And if at any time you want to switch providers, you won’t be tied into our architecture.

