+++
title = "Do you provide prediction models or algorithms?"
anchor = "do-you-provide-prediction-models-or-algorithms"
date = "2018-08-14T12:42:14+05:30"
type = "page-faq"
draft = false
+++

No, we do not offer algorithms or models. We strongly believe that the only way to successfully deploy machine learning applications is by combining domain expertise with statistical modeling. In other words, we don’t know your business well enough to model it for you.

However, should you need a hand with data science, modeling and algorithm expertise, we can connect you to trusted consulting partners. Upon discussing your business needs, we can provide suggestions as to what types of models and statistics you may likely need.

