+++
title = "I already have a behavioral tracking system, can I still use PrimedIO?"
anchor = "i-already-have-a-behavioral-tracking-system-can-i-still-use-primedio"
date = "2018-08-14T12:42:14+05:30"
type = "page-faq"
draft = false
+++

Yes you can. The tracking and delivery services are complementary, but can function separately without loss of functionality. In fact, one of our key advantages is being able to accommodate any business use case, architecture and data science practice - including blended AI and list-based models. Therefore, the platform is highly compatible with most existing and custom solutions.

Still, using our tracking component and the delivery component together makes for a smoother ride.

