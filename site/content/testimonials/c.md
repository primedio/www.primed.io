+++
title = "C"
anchor = "c"
name = "Geoffrey van Meer"
recommendedby = "Head of Data Intelligence, RTL"
src = '/images/enterprise-team-2.jpg'
date = "2018-08-14T12:42:14+05:30"
type = "page-testimonials"
draft = false
+++

The model blending feature allows us to manage the complexity of anticipating consumer behavior. The capability of blending statistical models with human curated lists enables the best of both worlds while avoiding the “AI takes over” narrative.