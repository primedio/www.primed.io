/*=========================================================
Scripts
=========================================================*/

//Sticky Nav
var $stickyMenu = $('.navbar-primed');
var stickyNavTop = $($stickyMenu).offset().top + 200;
var navHeight = $($stickyMenu).innerHeight();
var stickyNav = function(){
    var scrollTop = jQuery(window).scrollTop();
    if (scrollTop > stickyNavTop) {
        if ($(window).width() > 767) {
            $($stickyMenu).addClass('sticky');
            $('html').css('padding-top', navHeight + 'px');
        }
    } else {
        if ($(window).width() > 767) {
            $($stickyMenu).removeClass('sticky');
            $('html').css('padding-top', '0');
        }
    }
};
stickyNav();
$(window).scroll(function() {
    stickyNav();
});
//--------------------------------------------------------------


//Testimonials Slider
$('.testimonials-slider').slick({
    arrows: true,
    autoplay: true,
    autoplaySpeed: 3000,
    cssEase: 'linear',
    fade: true,
    slidesToShow: 1,
    speed: 300,
});

$('.testimonial-one').click(function(e) {
    $('.testimonials-slider').slick('slickGoTo', '1');
});

$('.testimonial-two').click(function(e) {
    $('.testimonials-slider').slick('slickGoTo', '2');
});

$('.testimonial-three').click(function(e) {
    $('.testimonials-slider').slick('slickGoTo', '3');
});

$('.testimonial-four').click(function(e) {
    $('.testimonials-slider').slick('slickGoTo', '4');
});

$('.testimonials-slider').on('beforeChange', function(event, slick, currentSlide){
    $('.testimonial-nav').removeClass('active');
    $('.testimonial-nav[data-id=' + (currentSlide) + ']').addClass('active');
  });

//--------------------------------------------------------------


//Mobile Nav
$('.navbar-toggle').click(function() {
    $('.navbar-primed #navbar').addClass('mobileMenuOpen');
    $('body').css('overflow','hidden');
});

$('.close-mobile-menu').click(function() {
    $('.navbar-primed #navbar').removeClass('mobileMenuOpen');
    $('body').css('overflow','');
});
//--------------------------------------------------------------


//Form
$("form .form-control").focus(function () {
    $(this).siblings().addClass('label-hover');
}).blur(function () {
    $(this).siblings().removeClass('label-hover');
});
//--------------------------------------------------------------


//Enterprise Infrastructure Map
var divs = $( '.map-dots .map-dot' );
var index = 0;


$('.map-marker').on('click', function(){
    $(this).toggleClass('marker-active');

    if ($(".map-marker").hasClass("marker-active")) {
        //Remove class from test location
        $('.test-location-time').removeClass('hidden');

        //Add Class to all map dots
        var delay = setInterval( function(){
        if ( index <= divs.length ){
            $( divs[ index ] ).addClass( 'animation' );
            index += 1;
        }else{
            clearInterval( delay );
        }
        }, 100 );

    } else {
        $('.test-location-time').addClass('hidden');
    }
});


//--------------------------------------------------------------


//Terminal
$('ul.backend-languages li > a').click(function() {
    $('ul.backend-languages li').removeClass('active');
    $(this).closest('li').addClass('active');
});


//Back End Languages
$('ul.frontend-languages li > a').click(function() {
    $('ul.frontend-languages li').removeClass('active');
    $(this).closest('li').addClass('active');
});

$('ul.backend-languages li.ruby > a').click(function() {
    var getLangName = $(this).find('span').text();
    $('.backend-terminal .code-area').fadeOut();
    $('.backend-terminal .code-area.ruby').fadeIn();
    $('.backend-terminal .back-end-name').text(getLangName);
});

$('ul.backend-languages li.rails > a').click(function() {
    var getLangName = $(this).find('span').text();
    $('.backend-terminal .code-area').fadeOut();
    $('.backend-terminal .code-area.rails').fadeIn();
    $('.backend-terminal .back-end-name').text(getLangName);
});

$('ul.backend-languages li.python > a').click(function() {
    var getLangName = $(this).find('span').text();
    $('.backend-terminal .code-area').fadeOut();
    $('.backend-terminal .code-area.python').fadeIn();
    $('.backend-terminal .back-end-name').text(getLangName);
});

$('ul.backend-languages li.django > a').click(function() {
    var getLangName = $(this).find('span').text();
    $('.backend-terminal .code-area').fadeOut();
    $('.backend-terminal .code-area.django').fadeIn();
    $('.backend-terminal .back-end-name').text(getLangName);
});

$('ul.backend-languages li.php > a').click(function() {
    var getLangName = $(this).find('span').text();
    $('.backend-terminal .code-area').fadeOut();
    $('.backend-terminal .code-area.php').fadeIn();
    $('.backend-terminal .back-end-name').text(getLangName);
});

$('ul.backend-languages li.symfony > a').click(function() {
    var getLangName = $(this).find('span').text();
    $('.backend-terminal .code-area').fadeOut();
    $('.backend-terminal .code-area.symfony').fadeIn();
    $('.backend-terminal .back-end-name').text(getLangName);
});

$('ul.backend-languages li.javascript > a').click(function() {
    var getLangName = $(this).find('span').text();
    $('.backend-terminal .code-area').fadeOut();
    $('.backend-terminal .code-area.javascript').fadeIn();
    $('.backend-terminal .back-end-name').text(getLangName);
});

$('ul.backend-languages li.java > a').click(function() {
    var getLangName = $(this).find('span').text();
    $('.backend-terminal .code-area').fadeOut();
    $('.backend-terminal .code-area.java').fadeIn();
    $('.backend-terminal .back-end-name').text(getLangName);
});

$('ul.backend-languages li.scala > a').click(function() {
    var getLangName = $(this).find('span').text();
    $('.backend-terminal .code-area').fadeOut();
    $('.backend-terminal .code-area.scala').fadeIn();
    $('.backend-terminal .back-end-name').text(getLangName);
});

$('ul.backend-languages li.go > a').click(function() {
    var getLangName = $(this).find('span').text();
    $('.backend-terminal .code-area').fadeOut();
    $('.backend-terminal .code-area.go').fadeIn();
    $('.backend-terminal .back-end-name').text(getLangName);
});

$('ul.backend-languages li.c > a').click(function() {
    var getLangName = $(this).find('span').text();
    $('.backend-terminal .code-area').fadeOut();
    $('.backend-terminal .code-area.c').fadeIn();
    $('.backend-terminal .back-end-name').text(getLangName);
});

$('ul.backend-languages li.swift > a').click(function() {
    var getLangName = $(this).find('span').text();
    $('.backend-terminal .code-area').fadeOut();
    $('.backend-terminal .code-area.swift').fadeIn();
    $('.backend-terminal .back-end-name').text(getLangName);
});

$('ul.backend-languages li.android > a').click(function() {
    var getLangName = $(this).find('span').text();
    $('.backend-terminal .code-area').fadeOut();
    $('.backend-terminal .code-area.android').fadeIn();
    $('.backend-terminal .back-end-name').text(getLangName);
});


//Front End Languages
$('ul.frontend-languages li.javascript > a').click(function() {
    var getLangName = $(this).find('span').text();
    $('.frontend-terminal .code-area').fadeOut();
    $('.frontend-terminal .code-area.javascript').fadeIn();
    $('.frontend-terminal .back-end-name').text(getLangName);
});

$('ul.frontend-languages li.react > a').click(function() {
    var getLangName = $(this).find('span').text();
    $('.frontend-terminal .code-area').fadeOut();
    $('.frontend-terminal .code-area.react').fadeIn();
    $('.frontend-terminal .back-end-name').text(getLangName);
});

$('ul.frontend-languages li.vue > a').click(function() {
    var getLangName = $(this).find('span').text();
    $('.frontend-terminal .code-area').fadeOut();
    $('.frontend-terminal .code-area.vue').fadeIn();
    $('.frontend-terminal .back-end-name').text(getLangName);
});

$('ul.frontend-languages li.angular > a').click(function() {
    var getLangName = $(this).find('span').text();
    $('.frontend-terminal .code-area').fadeOut();
    $('.frontend-terminal .code-area.angular').fadeIn();
    $('.frontend-terminal .back-end-name').text(getLangName);
});

$('ul.frontend-languages li.android > a').click(function() {
    var getLangName = $(this).find('span').text();
    $('.frontend-terminal .code-area').fadeOut();
    $('.frontend-terminal .code-area.android').fadeIn();
    $('.frontend-terminal .back-end-name').text(getLangName);
});

$('ul.frontend-languages li.swift > a').click(function() {
    var getLangName = $(this).find('span').text();
    $('.frontend-terminal .code-area').fadeOut();
    $('.frontend-terminal .code-area.swift').fadeIn();
    $('.frontend-terminal .back-end-name').text(getLangName);
});
//--------------------------------------------------------------