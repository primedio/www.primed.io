/*
Namespace for docs site
*/
var DocsTheme = {

  init: function() {
    this.initTabbedBlocks();
    this.initToc();
    this.initCopyCode();
    this.initCopyAnchors();
  },

    initTabbedBlocks: function() {
      // set up tabbed code blocks
      $('.tab-content').find('.tab-pane').each(function(idx, item) {
        var navTabs = $(this).closest('.code-tabs').find('.nav-tabs'),
            title = $(this).attr('title');
        navTabs.append('<li><a href="#">'+title+'</a></li');
      });

      $('.code-tabs ul.nav-tabs').each(function() {
        $(this).find("li:first").addClass('active');
      })

      $('.code-tabs .tab-content').each(function() {
        // $(item).find('tab-pane').addClass('active');
        $(this).find("div:first").addClass('active');
      });

      $('.nav-tabs a').click(function(e){
        e.preventDefault();
        var tab  = $(this).parent(),
            tabIndex = tab.index(),
            tabPanel = $(this).closest('.code-tabs'),
            tabPane = tabPanel.find('.tab-pane').eq(tabIndex);
        tabPanel.find('.active').removeClass('active');
        tab.addClass('active');
        tabPane.addClass('active');
      });

      // todo - optimize and make less terrible
      $('.code-tabs').each(function() {
        var largest = 0;
        var codeHeight = 0;
        var panes = $(this).find('.tab-pane');
        panes.each(function() {
          var outerHeight = $(this).outerHeight();
          console.log("outerHeight: " + outerHeight);
          if (outerHeight > largest) {
            largest = outerHeight;
            codeHeight = $(this).find('code').outerHeight();
          }
        });
        console.log("codeHeight: " + codeHeight);
        panes.each(function() {
          $(this).height(largest);
          // make all the <code> elements the same height to
          // avoid it jumping around when switching tabs
          $(this).find('code').height(largest - 5);
        });
      });
    },

    initCopyCode: function() {

      $('pre code').each(function() {
        var code = $(this);
        code.after('<span class="copy-to-clipboard">Copy</span>');
        code.on('mouseenter', function() {
          var copyBlock = $(this).next('.copy-to-clipboard');
          copyBlock.addClass('copy-active');
        });
        code.on('mouseleave', function() {
          var copyBlock = $(this).next('.copy-to-clipboard');
          copyBlock.removeClass('copy-active');
          copyBlock.html("Copy");
        });
      });

      var text, clip = new Clipboard('.copy-to-clipboard', {
        text: function(trigger) {
          return $(trigger).prev('code').text();
        }
      });

      clip.on('success', function(e) {
        e.clearSelection();
        console.log("copied!");
        $(e.trigger).html("Copied!");
      });

      clip.on('error', function(e) {
        console.log("error: " + e);
      });
    },

    initCopyAnchors: function() {
      $("h2").append(function(index, html){
        var element = $(this);
        var url = document.location.origin + document.location.pathname;
        var link = url + "#"+element[0].id;
        return " <span class='anchor'><a href='"+link+"'>" +
          "<i class='fa fa-link fa-lg'></i></a></span>"
        ;
      });
    },

    initToc: function() {
      // Hugo gives back a stupidly structured nav structure :shrug:
      var toc = $('#TableOfContents > ul > li > ul');
      toc.prepend('<li class="toc-label">Contents</li>')
    }
};

$(document).ready(function() {
  DocsTheme.init();
});
